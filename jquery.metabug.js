/* Metadata Debugger Console (MetaBug) v1.0
*
* Created by Kyle Knox <kknox@sireninteractive.com>
* Created on 11/09/2012
*
* Pulls metadata and other useful information from a page and displays it in a window for easy QA.
*
*/

(function($){
$.fn.metabug = function(options){
	var defaults = {
		'position': 'fixed',		// positions: fixed (floats on left-side of page) or top (static position above page content; only displays title, keywords and description)
		'infoMetadata': true,		// display page metadata (title, keywords, description, fb share image)
		'infoAnalytics': true,		// display analytics info (# events triggered and information about triggered events)
		'infoColor': false,			// colour sampler displays foreground/background colours (@todo)
		'infoTiming': true,			// display page timing info (load time, redirects, navigation method)
		'infoMemory': false			// display JS memory usage (Chrome only)
	};
	
	var settings = $.extend(defaults, options),
		dataMetadata = {},
		dataAnalytics = {},
		dataTiming = {},
		dataMemory = {};
	
	/* public functions */
	
	/* accessor function for parsed data */
	this._get = function(type) {
		var ret;
		
		switch(type) {
			case 'metadata':
				ret = dataMetadata;
			break;
			case 'analytics':
				ret = dataAnalytics;
			break;
			case 'memory':
				ret = dataMemory;
			break;
			case 'timing':
				ret = dataTiming;
			break;
			case 'settings':
			default:
				ret = settings;
			break;
		}
		
		return ret;
	}
	
	// for tracking analytics events
	// call externally when making call to google
	this.trackEvent = function (category, action, label) {
		if (!category) return;
		
		if (!action) {action = '';}
		if (!label) {label = '';}
		
		dataAnalytics.total++;
		dataAnalytics.history.push({"category":category,"action":action,"label":label});

		$('#metabug').find('#ga_total').text(dataAnalytics.total);
		$('#metabug').find('#ga_category').text(category);
		$('#metabug').find('#ga_action').text(action);
		$('#metabug').find('#ga_label').text(label);
	}
	
	this._destroy = function() {
		$('#metabug').remove();
	}
		
	return this.each(function(){
		var f = new metabug($(this), settings);
		f.init();
	});
	
/* main function */
function metabug(element) {
	this.render = render;
	this.init = init;
	
	var _element = element[0];
	
	// load all console data and render the console
	function init() {
		// defer rendering until after the page has finished loading
		if (typeof window.performance != 'undefined' && window.performance.timing.domComplete < 1) {
			setTimeout(function() {
				init();
			},750);
			return;
		}
		
		// load all data, even if it won't be displayed
		getMetadata();
		getTiming();
		getMemory();
		setAnalytics();
		
		render();
	}
	
	/* OUTPUT/RENDERING */
	
	// parse the information and display the console based on console settings
	function render() {
		if ($('#metabug').length>0) {
			$('#metabug').remove();
		}
		
		var ht;
		
		if (settings.position == 'top') {
			ht = '<div id="metabug"><div class="wrapper">';
		} else {
			ht = '<div id="metabug" class="fixed">\
				<a href="#" title="Toggle metadata viewer">';
			
			var bs = false;
			
			// if bootstrap is loaded, utilize those icons
			for (var i=0;i<document.styleSheets.length;i++) {
				if(/bootstrap/.test(document.styleSheets[i].href)) {
					bs = true;
					break;
				}
			}
			
			if (bs) {
				ht += '<i class="icon-chevron-right icon-white"></i>';
			} else {
				ht += '&times;';
			}
			
			ht += '</a><div class="wrapper">';
		}
		
		if (settings.infoMetadata) {
			ht += renderMetadata();
		}
		
		if (settings.position == 'fixed') {
			if (settings.infoAnalytics) {
				ht += renderAnalytics();
			}
			
			if (settings.infoTiming) {
				ht += renderTiming();
			}
			
			if (settings.infoMemory) {
				ht += renderMemory();
			}
		}
		
		ht += '</div>';
		
		$('body').prepend(ht);
		
		// toggle console event
		if (settings.position == 'fixed') {
			$('#metabug a').click(function(e){
				e.preventDefault();
				$(this).parent().find('div.wrapper').slideToggle('slow', function() {
					// opening
					if ($(this).is(':visible')) {
						$('#metabug a i').removeClass('icon-chevron-right').addClass('icon-remove');
						
						// absolutely position the console when it is open in case it is too tall for the screen
						$(this).parent().css('top',$(this).parent().offset().top+'px').addClass('sticky');
					} // closing
					else {
						$('#metabug a i').removeClass('icon-remove').addClass('icon-chevron-right');
						
						// switch back to fixed position for easy access
						$(this).parent().css('top','').removeClass('sticky');
					}
				});
			});
		}
	}
	
	// formats metadata
	function renderMetadata() {
		var ret = '';
		
		if (dataMetadata.title) {
			ret += formatRecord('Page Title', dataMetadata.title);
		}
		if (dataMetadata.keywords) {
			ret += formatRecord('Page Keywords', dataMetadata.keywords);
		}
		if (dataMetadata.description) {
			ret += formatRecord('Page Description', dataMetadata.description);
		}
		if(settings.position == 'fixed'){
			if (dataMetadata.fb_share) {
				ret += formatRecord('Facebook Share Image', dataMetadata.fb_share, 'image');
			} else {
				ret += formatRecord('Facebook Share Image', 'No image provided. Random image will be selected from the page.');
			}
		}
		
		return ret;
	}
	
	// formats analytics info
	function renderAnalytics() {
		
		var ret = '<h5>GA Events</h5>';
		
		// if analytics not loaded, don't bother
		if("_gaq" in window){
			ret += formatRecord('Triggered', 0, 'ga_triggered', true);
			ret += formatRecord('Last Triggered', '', 'ga_last', true);
		} else {
			ret += '<div class="record">Google Analytics not enabled</div>';
		}
		
		return ret;
	}
	
	// formats timing info
	function renderTiming() {
		var ret = '<h5>Page Timing</h5>';
		if (dataTiming.nav_type) {
			ret += formatRecord('Navigation Method', dataTiming.nav_type, 'string', true);
		}
		if (dataTiming.redirects){
			ret += formatRecord('Redirects', dataTiming.redirects, 'int', true);
		}
		if (dataTiming.dom_click){
			ret += formatRecord('DOM loaded (from click)', dataTiming.dom_click, 'ms', true);
		}
		if (dataTiming.onload_click){
			ret += formatRecord('onLoad fired (from click)', dataTiming.onload_click, 'ms', true);
		}
		if (dataTiming.dom_request){
			ret += formatRecord('DOM loaded (from request)', dataTiming.dom_request, 'ms', true);
		}
		if (dataTiming.onload_request){
			ret += formatRecord('onLoad fired (from request)', dataTiming.onload_request, 'ms', true);
		}
		
		return ret;
	}
	
	// formats memory usage info
	function renderMemory() {
		var ret = '<h5>Memory Usage</h5>';
		if(dataMemory.heap_size){
			ret += formatRecord('Memory (Heap Size)', dataMemory.heap_size, 'kB', true);
		}
		if(dataMemory.heap_used){
			ret += formatRecord('Memory (Heap Used)', dataMemory.heap_used, 'kB', true);
		}
		
		return ret;
	}
	
	/* DATA PARSING */
	
	// parse and store page metadata
	function getMetadata() {
		dataMetadata["title"] = $('title').html();
		dataMetadata["keywords"] = $('meta[name="keywords"]').attr('content');
		dataMetadata["description"] = $('meta[name="description"]').attr('content');
		dataMetadata["fb_share"] = $('meta[property="og:image"]').attr('content');
	}
	
	// parse and store timing info
	function getTiming() {
		if (typeof window.performance == 'undefined') return;
		
		var navStart = window.performance.timing.navigationStart,
			reqStart = window.performance.timing.requestStart,
			navType;
			
		// how did the user get to this page?
		switch(window.performance.navigation.type) {
			case window.performance.navigation.TYPE_NAVIGATE:
				navType = 'Navigation: clicked or entered URL';
			break;
			case window.performance.navigation.TYPE_RELOAD:
				navType = 'Page reload';
			break;
			case window.performance.navigation.TYPE_BACK_FORWARD:
				navType = 'Back/Forward button';
			break;
			case window.performance.navigation.TYPE_RESERVED:
				navType = 'Other';
			break;
			default:
				navType = 'Unknown';
			break;
		}
		
		dataTiming["nav_type"] = navType;
		
		// # number of redirects to get to current page
		dataTiming["redirects"] = window.performance.navigation.redirectCount;
		
		// time since the user clicked mouse to initiate request (in ms)
		dataTiming["dom_click"] = window.performance.timing.domComplete-navStart;
		dataTiming["onload_click"] = window.performance.timing.loadEventStart-navStart;
		
		// time since request was initiated (in ms)
		dataTiming["dom_request"] = window.performance.timing.domComplete-reqStart;
		dataTiming["onload_request"] = window.performance.timing.loadEventStart-reqStart;
	}
	
	// parse and store memory usage info (in kB)
	function getMemory() {
		if (typeof window.performance.memory == 'undefined') return;
		
		dataMemory["heap_size"] = window.performance.memory.totalJSHeapSize/8000;
		dataMemory["heap_used"] = window.performance.memory.usedJSHeapSize/8000;
	}
	
	function setAnalytics() {
		dataAnalytics["total"] = 0;
		dataAnalytics["history"] = [];
	}
	
	/* UTILITY FUNCTIONS */
	
	// format individual data records
	/* @heading - title of section
	*  @val - section value
	*  @type - data-type of @val
	*  @inline - set to true to display @heading and @val on same line
	*/
	function formatRecord(heading, val, type, inline) {
		type = typeof type !== 'string' ? 'string' : type;
		inline = typeof inline !== 'boolean' ? false : inline;
		
		var ret,fv;

		if (inline) {
			ret = '<div class="record inline clearfix">';
		} else {
			ret = '<div class="record">';
		}
		
		switch(type) {
			case 'image':
				var dim = getDimensions(val);
				
				fv = '<div class="image">';
				fv += '<img src="'+val+'" alt="'+heading+'"/>';
				fv += '<div class="dimensions">'+dim.width+' x '+dim.height+'</div>';
				fv += '</div>';
			break;
			case 'string':
				fv = val;
			break;
			case 'int':
				fv = parseInt(val, 10);
			break;
			case 'ga_triggered':
				fv = '<span id="ga_total">'+val+'</span>';
			break;
			case 'ga_last':
				fv = '<span id="ga_category">'+val+'</span> &ndash; <span id="ga_action">'+val+'</span> &ndash; <span id="ga_label">'+val+'</span>';
			break;
			default:
				fv = val+' '+type;
			break;
		}
		
		if (inline) {
			ret += '<span class="heading">'+heading+':</span>';
			ret += fv;
		} else {
			ret += '<h5>'+heading+'</h5>';
			ret += '<p>'+fv+'</p>';
		}
		
		ret += '</div>';
		
		return ret;
	}
	
	// retrieves image dimensions
	function getDimensions(img) {
		var io = new Image();
		io.src = img;
		
		return {"height":io.height,"width":io.width};
	}
}};
})(jQuery);