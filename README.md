# Metadata Debugger Console (Metabug)  
Version 1.0

This jQuery plugin creates a graphical debugging console which parses a page to display metadata, load time, and other useful information which exists outside of the HTML body. It can be used for QA or dev testing, or for client approval on staging sites.

## HOW TO USE

Using the autoload script:

- Copy the metabug folder to the website.
- Add a **<script>** tag for *metabug/jquery.metabug.autoload.js* before the closing **</body>** tag
- Open the console using the shortcut <*ctrl+m*>, or using the following code:  
$('body').metabug({options});

Manual installation/initialisation:

- Add the Metabug script and stylesheet to the site and link to these resources in the head.
- The console can be initialzed on page load, or from the JS console with the following:  
$('body').metabug({options});
	
Requirements: jQuery (v1.6+)

## OPTIONS

Below are the available options for modifying the behaviour and information displayed by Metabug. Thes are set when the console is first initialized:

- **position**: fixed (default) | top
	Sets where the console is displayed. When set to 'fixed', the console is fixed to the left-side of the window as a tiny black box, scrolling with the page, which slides out to reveal its info. Top-positioning prepends the console to the top of the page (before the header) and only displays basic metadata (title, keywords and description).
	
- **infoMetadata**: true (default) | false
	Display page metadata: title, keywords, description and Facebook share image
	
- **infoTiming**: true | false (default)
	Display page timing information, if available. This displays load time, number of redirects in the user's request, and the navigation method to the current page.
	
- **infoMemory**: true | false (default)
	Display JavaScript memory usage information (Chrome only).
	
- **infoAnalytics**: true | false (default)
	Display number of Google analytics events fired, and details about last event fired. Enable by calling Metabug's `trackEvent()` method when passing the data to analytics' _trackEvent() method. Not fully implemented/supported.

- **infoColor**: true | false (default)
	#Coming soon

## PUBLIC METHODS

- obj `_get(string:type)`
	Returns the data associated with the page.
	Valid values for @type are: settings (default), metadata, analytics, timing, and memory

- void `_destroy(void)`
	Removes the console from the DOM.
	
- void `trackEvent(string:category, string:action = '', string:label = '')`
	Updates analytics data in console. 
	
## TODO - Future Stuff

- track analytics events
- colour eyedropper tool