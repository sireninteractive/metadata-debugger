var mb_console;

$(document).ready(function(){
	var dir = '/js/metabug/';
	
	// load the stylesheet, if it's not already loaded
	if(!$('link[href$="jquery.metabug.css"]').length) {
		$('head').append('<link rel="stylesheet" type="text/css" href="'+dir+'jquery.metabug.css"/>');
	}
	
	// load the script, if it's not already loaded
	if(!$('script[src$="jquery.metabug.js"]').length) {
		$('body').append('<script type="text/javascript" src="'+dir+'jquery.metabug.js"/>');
	}

	$('body').keypress(function(e){
		// shortcut key: ctrl+m
		if (e.ctrlKey && e.which == 109) {
			mb_console= $('body').metabug();
		}
	});
});
